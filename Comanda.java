package models;

public class Comanda {
    private int id;
    private int personId;
    private int productId;
    private int quantity;
    private int price;

    public Comanda(){}

    public Comanda(int id, int personId, int productId, int quantity, int price){
      this.id = id;
      this.personId = personId;
      this.productId = productId;
      this.quantity = quantity;
      this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductId() {
        return productId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    @Override
    public String toString() {
        return "Client Id:\t\t" + this.personId +
                "\nProduct ID:\t\t" + this.productId +
                "\nQuantity:\t\t" + this.quantity +
                "\nPrice:\t\t" + this.price;
    }
}
