package dao;

import models.Comanda;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ComandaDAO extends AbstractDAO {

    public Comanda selectById(int id){
        Comanda comanda = null;
        String query = "SELECT * FROM " + Comanda.class.getSimpleName() + " WHERE id = " + id + ";"; //returneaza comanda cu id ul id
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            resultSet = preparedStatement.executeQuery(); //rezultatele in urma executarii query ului
            while (resultSet.next()){
                comanda = new Comanda((Integer)resultSet.getObject("id"),
                        (Integer)resultSet.getObject("personId"),
                        (Integer)resultSet.getObject("productId"),
                        (Integer)resultSet.getObject("quantity"),
                        (Integer)resultSet.getObject("price"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
        return comanda;
    }

    public ArrayList<Comanda> selectAll(){
        ArrayList<Comanda> comandas = new ArrayList<>();
        String query = "SELECT * FROM " + Comanda.class.getSimpleName() + ";";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                comandas.add(new Comanda((Integer)resultSet.getObject("id"),
                        (Integer)resultSet.getObject("personId"),
                        (Integer)resultSet.getObject("productId"),
                        (Integer)resultSet.getObject("quantity"),
                        (Integer)resultSet.getObject("price")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
        return comandas;
    }

    public void insert(Comanda comanda){
        String query = "INSERT INTO " + Comanda.class.getSimpleName() + "( productId, personId, quantity, price )"+
                " VALUES ( " + comanda.getProductId() +", " + comanda.getPersonId() + ", " +
                comanda.getQuantity() + ", " + comanda.getPrice() + ");";
        connection.openConnection();

        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }

    public void delete(int id){
       String query = "DELETE FROM " + Comanda.class.getSimpleName() + " WHERE id = " + id;
        connection.openConnection();

        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }
}
