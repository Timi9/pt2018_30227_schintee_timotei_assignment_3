package bll;

import dao.*;
import models.*;

import java.util.ArrayList;
import java.util.List;

public class PersonBLL {
    private PersonDAO personDAO;

    public PersonBLL(){
        personDAO = new PersonDAO();
    }

    public void insert(Person person){
        if (person == null)
        return;
        if(person.getLastName()==null || person.getLastName().length()==0)
            return;
        if(person.getFirstName()==null || person.getFirstName().length()==0)
            return;
        personDAO.insert(person);
    }

    public Person selectById(int id){
        return personDAO.selectById(id);
    }

    public ArrayList<Person> selectAll(){
        return personDAO.selectAll();
    }

    public void delete(Person person){
        if (person == null)
            return;
        personDAO.delete(person.getId());
    }

    public void  update(Person person) {
        if (person == null)
            return;
        if(person.getId()<1)
            return;
        if(person.getLastName()==null || person.getLastName().length()==0)
            return;
        if(person.getFirstName()==null || person.getFirstName().length()==0)
            return;
        personDAO.update(person);
    }
}