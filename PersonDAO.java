package dao;

import models.Person;

import java.sql.SQLException;
import java.util.ArrayList;

public class PersonDAO extends AbstractDAO {

    public Person selectById(int id) {
        Person person = null;
        String query = "SELECT * FROM " + Person.class.getSimpleName() + " WHERE id = " + id + ";";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                person = new Person((Integer) resultSet.getObject("id"),
                        (String) resultSet.getObject("firstName"),
                        (String) resultSet.getObject("lastName")
                        );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
        return person;
    }

    public ArrayList<Person> selectAll() {
        ArrayList<Person> personList = new ArrayList<>();
        String query = "SELECT * FROM " + Person.class.getSimpleName() + ";";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                personList.add(new Person((Integer) resultSet.getObject("id"),
                        (String) resultSet.getObject("firstName"),
                        (String) resultSet.getObject("lastName")));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
        return personList;
    }

    public void insert(Person person) {
        String query = "INSERT INTO " + Person.class.getSimpleName() + "( firstName, lastName )" +
                " VALUES ( '" + person.getFirstName() + "', " + "'" +person.getLastName() + "'); ";
        connection.openConnection();

        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }

    public void delete(int id) {
        String query = "DELETE FROM " + Person.class.getSimpleName() + " WHERE id = " + id+ ";";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }

    public void update(Person person) {
        String query = "UPDATE " + Person.class.getSimpleName()
                + " SET firstName =" + " '" + person.getFirstName()
                + "', " + "lastName = " + "'" +person.getLastName() + "'" +
                " WHERE  id = " + person.getId() +"; ";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }
}