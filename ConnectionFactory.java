package dao;

import java.sql.*;

public class ConnectionFactory {
    private static final String USER = "root";
    private static final String PASSWORD = "";
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DB_URL = "jdbc:mysql://localhost/Warehouse3?useSSL=false&"+
            "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static Connection conn;
    private static ConnectionFactory connectionFactory = null;

    private ConnectionFactory(){
        openConnection();
    }

    public void openConnection(){
        try {
            Class.forName(DRIVER).newInstance();
            this.conn = DriverManager.getConnection(
                    DB_URL, USER, PASSWORD);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public static ConnectionFactory getConnection() {
        if(connectionFactory == null){
            connectionFactory = new ConnectionFactory();
        }
        return connectionFactory;
    }

    public  Connection getConn() {
        return conn;
    }

    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
            }
        }
    }

    public static void close(PreparedStatement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
            }
        }
    }

    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
            }
        }
    }


}
