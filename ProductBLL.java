package bll;

import dao.ProductDAO;
import models.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductBLL {
    private ProductDAO productDAO;

    public ProductBLL(){
        productDAO = new ProductDAO();
    }

    public void insert(Product product){
        if ( product == null)
            return;
        if(product.getQuantity()<0)
            return;
        if(product.getPrice() < 1)
            return;
        productDAO.insert(product);
    }

    public Product selectById(int id){
        return productDAO.selectById(id);
    }

    public ArrayList<Product> selectAll(){
        return productDAO.selectAll();
    }

    public void delete(Product product){
        if(product == null)
            return;
        productDAO.delete(product.getId());
    }

    public void  update(Product product) {
        if(product.getId()<1)
            return;
        if(product.getQuantity()<0)
            return;
        if(product.getPrice() < 1)
            return;
        productDAO.update(product);
    }
}
