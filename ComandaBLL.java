package bll;

import dao.ComandaDAO;
import dao.ProductDAO;
import models.Comanda;
import models.Product;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ComandaBLL {

    private ComandaDAO comandaDAO;
    private ProductDAO productDAO;

    public ComandaBLL(){
        comandaDAO = new ComandaDAO();
        productDAO = new ProductDAO();
    }

    public void insert(Comanda comanda){
        if ( comanda == null)
            return;
        if (comanda.getPrice() < 0)
            return;
        if (comanda.getQuantity() < 0)
            return;
        Product product = productDAO.selectById(comanda.getProductId());
        if(product.getQuantity()< comanda.getQuantity())
        {
            JOptionPane.showMessageDialog(null, "Nu sunt suficiente produse");
            return;
        }
        product.setQuantity(product.getQuantity()- comanda.getQuantity());
        productDAO.update(product);
        comanda.setPrice(product.getPrice()*comanda.getQuantity());
        comandaDAO.insert(comanda);
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("Comanda.txt"));
            bufferedWriter.write(comanda.toString()+"\n");
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Comanda selectById(int id){
        return comandaDAO.selectById(id);
    }

    public ArrayList<Comanda> selectAll(){

        return comandaDAO.selectAll();
    }

    public void delete(Comanda comanda){
        if ( comanda == null )
            return;
        comandaDAO.delete(comanda.getId());
    }
}
