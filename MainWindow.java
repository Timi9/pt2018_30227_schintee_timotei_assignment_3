package presentation;

import models.Comanda;
import models.Person;
import models.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class MainWindow extends JFrame {
    public JPanel mainPanel, personPanel, comandaPanel, productPanel,
            personTablePanel, productTablePanel, comandaTablePanel,
            comandaOperationsPanel, productOperationsPanel, personOperationsPanel;
    public JTable personTable, productTable, comandaTable;
    public JTextField personFirstNameTextField, personLastNameTextField, productNameTextField, productPriceTextField, productQuantityTextField, orderQuantityTextField;
    public JButton deletePersonButton, editPersonButton, createPersonButton, deleteComandaButton, createComandaButton, deleteProductButton, createProductButton, editProductButton;
    public JLabel personSelected = new JLabel("Not Selected");
    public JLabel productSelected = new JLabel("Not Selected");
    public JLabel comandaSelected =  new JLabel("Not Selected");
    public MainWindow(){

        personSelected.setForeground(Color.RED);
        productSelected.setForeground(Color.RED);
        comandaSelected.setForeground(Color.RED);

        this.setSize(new Dimension(1200, 850));
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mainPanel = new JPanel();
        mainPanel.setPreferredSize(new Dimension(1200, 850));

        personPanel = new JPanel();
        personPanel.setPreferredSize(new Dimension(550, 400));

        personTablePanel = new JPanel();
        personTablePanel.setPreferredSize(new Dimension(300, 400));

        Object data[][] = new Object[0][Person.class.getDeclaredFields().length];
        Object header[] = new Object[Person.class.getDeclaredFields().length];
        int i = 0;
        for(Field field : Person.class.getDeclaredFields())
        {
            header[i++] = field.getName();
        }
        DefaultTableModel defaultTableModel = new DefaultTableModel(data, header);
        personTable = new JTable(defaultTableModel);
        JScrollPane jScrollPane = new JScrollPane(personTable);
        jScrollPane.setPreferredSize(new Dimension(280, 400));
        personTablePanel.add(jScrollPane);
        personPanel.add(personTablePanel);

        personOperationsPanel = new JPanel();
        personOperationsPanel.setPreferredSize(new Dimension(150,400));

        personFirstNameTextField = new JTextField("FirstName");
        personFirstNameTextField.setPreferredSize(new Dimension(150,30));
        personOperationsPanel.add(personFirstNameTextField);

        personLastNameTextField = new JTextField("LastName");
        personLastNameTextField.setPreferredSize(new Dimension(150,30));
        personOperationsPanel.add(personLastNameTextField);

        deletePersonButton = new JButton("DeletePerson");
        deletePersonButton.setPreferredSize(new Dimension(150,30));
        personOperationsPanel.add(deletePersonButton);

        editPersonButton = new JButton("EditPerson");
        editPersonButton.setPreferredSize(new Dimension(150,30));
        personOperationsPanel.add(editPersonButton);

        createPersonButton = new JButton("CreatePerson");
        createPersonButton.setPreferredSize(new Dimension(150,30));
        personOperationsPanel.add(createPersonButton);
        personOperationsPanel.add(personSelected);
        personPanel.add(personOperationsPanel);

        mainPanel.add(personPanel);

        productPanel = new JPanel();
        productPanel.setPreferredSize(new Dimension(600, 400));

        productTablePanel = new JPanel();
        productTablePanel.setPreferredSize(new Dimension(400, 400));

         data = new Object[0][Product.class.getDeclaredFields().length];
         header = new Object[Product.class.getDeclaredFields().length];
         i = 0;
        for(Field field : Product.class.getDeclaredFields())
        {
            header[i++] = field.getName();
        }
        DefaultTableModel defaultTableModel1 = new DefaultTableModel(data, header);
        productTable = new JTable(defaultTableModel1);
        JScrollPane jScrollPane1 = new JScrollPane(productTable);
        jScrollPane1.setPreferredSize(new Dimension(380, 400));
        productTablePanel.add(jScrollPane1);
        productPanel.add(productTablePanel);

        productOperationsPanel = new JPanel();
        productOperationsPanel.setPreferredSize(new Dimension(150,400));

        productNameTextField = new JTextField("Name");
        productNameTextField.setPreferredSize(new Dimension(150,30));
        productOperationsPanel.add(productNameTextField);

        productPriceTextField = new JTextField("Price");
        productPriceTextField.setPreferredSize(new Dimension(150,30));
        productOperationsPanel.add(productPriceTextField);

        productQuantityTextField = new JTextField("Quantity");
        productQuantityTextField.setPreferredSize(new Dimension(150,30));
        productOperationsPanel.add(productQuantityTextField);

        deleteProductButton = new JButton("DeleteProduct");
        deleteProductButton.setPreferredSize(new Dimension(150,30));
        productOperationsPanel.add(deleteProductButton);

        editProductButton = new JButton("EditProduct");
        editProductButton.setPreferredSize(new Dimension(150,30));
        productOperationsPanel.add(editProductButton);

        createProductButton = new JButton("CreateProduct");
        createProductButton.setPreferredSize(new Dimension(150,30));
        productOperationsPanel.add(createProductButton);
        productOperationsPanel.add(productSelected);

        productPanel.add(productOperationsPanel);

        mainPanel.add(productPanel);

        comandaPanel = new JPanel();
        comandaPanel.setPreferredSize(new Dimension(1150, 380));

        comandaTablePanel = new JPanel();
        comandaTablePanel.setPreferredSize(new Dimension(700, 350));

        data = new Object[0][Comanda.class.getDeclaredFields().length];
        header = new Object[Comanda.class.getDeclaredFields().length];
        i = 0;
        for(Field field : Comanda.class.getDeclaredFields())
        {
            header[i++] = field.getName();
        }
        DefaultTableModel defaultTableModel2 = new DefaultTableModel(data, header);
        comandaTable = new JTable(defaultTableModel2);
        JScrollPane jScrollPane2 = new JScrollPane(comandaTable);
        jScrollPane2.setPreferredSize(new Dimension(650, 350));
        comandaTablePanel.add(jScrollPane2);
        comandaPanel.add(comandaTablePanel);

        comandaOperationsPanel = new JPanel();
        comandaOperationsPanel.setPreferredSize(new Dimension(400,350));

        orderQuantityTextField = new JTextField("Quantity");
        orderQuantityTextField.setPreferredSize(new Dimension(400,30));
        comandaOperationsPanel.add(orderQuantityTextField);

        deleteComandaButton = new JButton("DeleteComanda");
        deleteComandaButton.setPreferredSize(new Dimension(400,30));
        comandaOperationsPanel.add(deleteComandaButton);

        createComandaButton = new JButton("CreateComanda");
        createComandaButton.setPreferredSize(new Dimension(400,30));
        comandaOperationsPanel.add(createComandaButton);
        comandaOperationsPanel.add(comandaSelected);

        comandaPanel.add(comandaOperationsPanel);

        mainPanel.add(comandaPanel);
        this.add(mainPanel);
    }

    public void  fillPersonTable(ArrayList<Person> people){
        Object header[] = new Object[Person.class.getDeclaredFields().length];
        int j =0;
        for (Field field: Person.class.getDeclaredFields()) {
            TableColumn tableColumn = new TableColumn();
            tableColumn.setHeaderValue(field.getName());
            header[j++] = field.getName();
        }

        Object data[][] = new Object[people.size()][Person.class.getDeclaredFields().length];
        j = 0;
        for (Person person: people){
            int i =0;
            for(Field field: person.getClass().getDeclaredFields()){
                field.setAccessible(true);
                try {
                    data[j][i] = field.get(person);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                ++i;
            }
            ++j;
        }
        DefaultTableModel defaultTableModel = new DefaultTableModel(data, header);
        personTable.setModel(defaultTableModel);
    }
    public void  fillProductTable(ArrayList<Product> products){
        Object header[] = new Object[Product.class.getDeclaredFields().length];
        int j =0;
        for (Field field: Product.class.getDeclaredFields()) {
            TableColumn tableColumn = new TableColumn();
            tableColumn.setHeaderValue(field.getName());
            header[j] = field.getName();
            j++;
        }

        Object data[][] = new Object[products.size()][Product.class.getDeclaredFields().length];
        j = 0;
        for (Product product: products){
            int i =0;
            for(Field field: product.getClass().getDeclaredFields()){
                field.setAccessible(true);
                try {
                    data[j][i] = field.get(product);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                ++i;
            }
            ++j;
        }
        DefaultTableModel defaultTableModel = new DefaultTableModel(data, header);
        productTable.setModel(defaultTableModel);
    }
    public void  fillComandaTable(ArrayList<Comanda> comandas){
        Object header[] = new Object[Comanda.class.getDeclaredFields().length];
        int j =0;
        for (Field field: Comanda.class.getDeclaredFields()) {
            TableColumn tableColumn = new TableColumn();
            tableColumn.setHeaderValue(field.getName());
            header[j] = field.getName();
            j++;
        }
        Object data[][] = new Object[comandas.size()][Comanda.class.getDeclaredFields().length];
        j = 0;
        for (Comanda comanda: comandas){
            int i =0;
            for(Field field: comanda.getClass().getDeclaredFields()){
                field.setAccessible(true);
                try {
                    data[j][i] = field.get(comanda);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                ++i;
            }
            ++j;
        }
        DefaultTableModel defaultTableModel = new DefaultTableModel(data, header);
        comandaTable.setModel(defaultTableModel);
    }

}
