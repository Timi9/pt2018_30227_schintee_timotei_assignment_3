package presentation;

import bll.ComandaBLL;
import bll.PersonBLL;
import bll.ProductBLL;
import models.Comanda;
import models.Person;
import models.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.LocalDate;

public class Controller {
    private MainWindow mainWindow;
    private ComandaBLL comandaBLL;
    private Comanda comandaCurenta = null;
    private PersonBLL personBLL;
    private Person persoanaCurenta = null;
    private ProductBLL productBLL;
    private Product produsCurent = null;

    public Controller(){
        mainWindow = new MainWindow();
        comandaBLL = new ComandaBLL();
        productBLL = new ProductBLL();
        personBLL = new PersonBLL();
        mainWindow.setVisible(true);
        mainWindow.fillPersonTable(personBLL.selectAll());
        mainWindow.fillProductTable(productBLL.selectAll());
        mainWindow.fillComandaTable(comandaBLL.selectAll());
        addTableListeners();
        addDeleteButtonsListeners();
        addCreateButtonListeners();
        addEditButtonsListener();

    }

    public void addEditButtonsListener(){
        mainWindow.editPersonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Person person = new Person();
                    person.setFirstName(mainWindow.personFirstNameTextField.getText());
                    person.setLastName(mainWindow.personLastNameTextField.getText());
                    person.setId(persoanaCurenta.getId());
                    personBLL.update(person);
                    persoanaCurenta = null;
                    mainWindow.personSelected.setText("Not Selected");
                    mainWindow.personSelected.setForeground(Color.RED);
                    mainWindow.fillPersonTable(personBLL.selectAll());
                } catch (Exception e1){}
            }
        });
        mainWindow.editProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Product product = new Product();
                    product.setNume(produsCurent.getNume());
                    product.setQuantity(Integer.parseInt(mainWindow.productQuantityTextField.getText()));
                    product.setPrice(Integer.parseInt(mainWindow.productPriceTextField.getText()));
                    product.setId(produsCurent.getId());
                    productBLL.update(product);
                    produsCurent = null;
                    mainWindow.productSelected.setText("Not Selected");
                    mainWindow.productSelected.setForeground(Color.RED);
                    mainWindow.fillProductTable(productBLL.selectAll());
                } catch (Exception e1){}
            }
        });
    }
    public void addCreateButtonListeners(){
        mainWindow.createComandaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Comanda comanda = new Comanda();
                    comanda.setPersonId(persoanaCurenta.getId());
                    comanda.setProductId(produsCurent.getId());
                    comanda.setQuantity(Integer.parseInt(mainWindow.orderQuantityTextField.getText()));
                    comandaBLL.insert(comanda);
                    persoanaCurenta = null;
                    produsCurent = null;
                    mainWindow.personSelected.setText("Not Selected");
                    mainWindow.personSelected.setForeground(Color.RED);
                    mainWindow.productSelected.setText("Not Selected");
                    mainWindow.productSelected.setForeground(Color.RED);
                    mainWindow.fillComandaTable(comandaBLL.selectAll());
                    mainWindow.fillProductTable(productBLL.selectAll());
                } catch (Exception e1){}
            }
        });
        mainWindow.createPersonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Person person = new Person();
                    person.setFirstName(mainWindow.personFirstNameTextField.getText());
                    person.setLastName(mainWindow.personLastNameTextField.getText());
                    personBLL.insert(person);
                    persoanaCurenta = null;
                    mainWindow.personSelected.setText("Not Selected");
                    mainWindow.personSelected.setForeground(Color.RED);
                    mainWindow.fillPersonTable(personBLL.selectAll());
                } catch (Exception e1){}
            }
        });
        mainWindow.createProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Product product = new Product();
                    product.setNume(mainWindow.productNameTextField.getText());
                    product.setQuantity(Integer.parseInt(mainWindow.productQuantityTextField.getText()));
                    product.setPrice(Integer.parseInt(mainWindow.productPriceTextField.getText()));
                    productBLL.insert(product);
                    produsCurent = null;
                    mainWindow.productSelected.setText("Not Selected");
                    mainWindow.productSelected.setForeground(Color.RED);
                    mainWindow.fillProductTable(productBLL.selectAll());
                } catch (Exception e1){}
            }
        });
    }
    public void addDeleteButtonsListeners(){
        mainWindow.deleteComandaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                comandaBLL.delete(comandaCurenta);
                comandaCurenta = null;
                mainWindow.comandaSelected.setText("Not Selected");
                mainWindow.comandaSelected.setForeground(Color.RED);
                mainWindow.fillComandaTable(comandaBLL.selectAll());
            }
        });
        mainWindow.deleteProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                productBLL.delete(produsCurent);
                produsCurent = null;
                mainWindow.productSelected.setText("Not Selected");
                mainWindow.productSelected.setForeground(Color.RED);
                mainWindow.fillProductTable(productBLL.selectAll());
            }
        });
        mainWindow.deletePersonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                personBLL.delete(persoanaCurenta);
                persoanaCurenta = null;
                mainWindow.personSelected.setText("Not Selected");
                mainWindow.personSelected.setForeground(Color.RED);
                mainWindow.fillPersonTable(personBLL.selectAll());
            }
        });
    }
    public void addTableListeners(){
        mainWindow.personTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable target = (JTable)e.getSource();
                int row = target.getSelectedRow();
                Person person = new Person((Integer)mainWindow.personTable.getValueAt(row, 0),
                        (String)mainWindow.personTable.getValueAt(row, 1),
                        (String)mainWindow.personTable.getValueAt(row, 2));
                persoanaCurenta = person;
                mainWindow.personSelected.setText("Selected");
                mainWindow.personSelected.setForeground(Color.GREEN);
                mainWindow.repaint();

            }
        });
        mainWindow.comandaTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable target = (JTable)e.getSource();
                int row = target.getSelectedRow();
                Comanda comanda = new Comanda((Integer)mainWindow.comandaTable.getValueAt(row, 0),
                        (Integer) mainWindow.comandaTable.getValueAt(row, 1),
                        (Integer) mainWindow.comandaTable.getValueAt(row, 2),
                        (Integer) mainWindow.comandaTable.getValueAt(row, 3),
                        (Integer) mainWindow.comandaTable.getValueAt(row, 4));
                comandaCurenta = comanda;
                mainWindow.comandaSelected.setText("Selected");
                mainWindow.comandaSelected.setForeground(Color.GREEN);
                mainWindow.repaint();
            }
        });
        mainWindow.productTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable target = (JTable)e.getSource();
                int row = target.getSelectedRow();
                Product product = new Product((Integer)mainWindow.productTable.getValueAt(row, 0),
                        (String) mainWindow.productTable.getValueAt(row, 1),
                        (Integer) mainWindow.productTable.getValueAt(row, 2),
                        (Integer) mainWindow.productTable.getValueAt(row, 3));
                produsCurent = product;
                mainWindow.productSelected.setText("Selected");
                mainWindow.productSelected.setForeground(Color.GREEN);
                mainWindow.repaint();
            }
        });
    }
}
