package dao;

import models.Person;
import models.Product;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO extends AbstractDAO {

    public Product selectById(int id) {
        Product product = null;
        String query = "SELECT * FROM " + Product.class.getSimpleName() + " WHERE id = " + id + ";";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                product = new Product((Integer) resultSet.getObject("id"),
                        (String) resultSet.getObject("nume"),
                        (Integer) resultSet.getObject("price"),
                        (Integer) resultSet.getObject("quantity")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
        return product;
    }

    public ArrayList<Product> selectAll() {
        ArrayList<Product> products = new ArrayList<>();
        String query = "SELECT * FROM " + Product.class.getSimpleName() + ";";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                products.add( new Product((Integer) resultSet.getObject("id"),
                        (String) resultSet.getObject("nume"),
                        (Integer) resultSet.getObject("price"),
                        (Integer) resultSet.getObject("quantity")
                ));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
        return products;
    }

    public void insert(Product product) {
        String query = "INSERT INTO " + Product.class.getSimpleName() + "( nume, price, quantity )" +
                " VALUES ( '" + product.getNume() +"', " + product.getPrice()+ ", " +
                product.getQuantity() + "); ";
        connection.openConnection();

        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }

    public void delete(int id) {
        String query = "DELETE FROM " + Product.class.getSimpleName() + " WHERE id = " + id;
        connection.openConnection();

        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }

    public void update(Product product) {
        String query = "UPDATE " + Product.class.getSimpleName()
                + " SET nume = " + " '" + product.getNume() + "', "
                + " price = "  + product.getPrice()
                + ", quantity = " + product.getQuantity() +
                " WHERE  id = " + product.getId()+";";
        connection.openConnection();
        try {
            preparedStatement = connection.getConn().prepareStatement(query);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection.getConn());
        }
    }
}
