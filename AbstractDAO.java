package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract class AbstractDAO {

    protected ConnectionFactory connection = null;
    protected ResultSet resultSet = null;
    protected PreparedStatement preparedStatement = null;

    public AbstractDAO(){
        connection = ConnectionFactory.getConnection();
    }
}
